defmodule Generator.Utils do
  @moduledoc """
  Tools class
  """

  @doc """
  Return `String`: The `text` with first letter being an uppercase, leave the rest as it was
  """
  @spec capitalize(text::String) :: String
  def capitalize(text) do     
    (String.slice(text, 0..0) |> String.upcase) <> String.slice(text, 1..(String.length text))           
  end

  @doc """
  Delete the file related to git (.git, .gitignore) at the `path` provided
  Returns `{:ok, :path}` path on success and `{:error, reason}
  """
  @spec clean_git_files(path::String) :: {:ok} | {:error, String}
  def clean_git_files(path) do
    with {:ok, _} <- File.rm_rf(Path.join path, ".git"), 
      _ <- delete_gitignore(path)
    do
      {:ok}
    else
      {:error, reason} ->  {:error, reason} 
    end
  end

  def delete_gitignore(path) do
    Path.wildcard(Path.join(path, "**/*gitignore"), match_dot: true) |> Enum.map(&File.rm &1)
  end

  @doc """
  If the file (`path`) is reguar it will replace every occurence `pattern` by `replacement`
  And in eithier case it the filename contain the `pattern` it will rename it with the `replacement`
  Return `String`: The new path of the file
  """
  @spec change_application_name(path::String, pattern:: String, replacement::String) :: String
  def change_application_name(path, pattern, replacement) do
    rename_file(path,pattern, replacement)
    |> rename_file_and_content(pattern, replacement, File.regular? path)
  end

  @spec rename_file(path::String, pattern::String, replacement::String) :: String
  defp rename_file(path, pattern, replacement) do
    path = cond do
      String.contains? path, pattern ->
        destination =  String.replace path, pattern, replacement
        File.rename path, destination
        destination
      String.contains? path, capitalize(pattern) ->
        destination = String.replace path, capitalize(pattern), capitalize(replacement)
        File.rename path, destination
        destination
      true -> path
    end    
    path
  end

  @spec rename_file_and_content(path::String, pattern:: String, replacement::String, :boolean) :: String
  defp rename_file_and_content(path, pattern, replacement, true) do
    File.write path,
      File.read!(path)
        |> String.replace(pattern, replacement)
        |> String.replace(capitalize(pattern), capitalize(replacement))
    path  
  end

  @spec rename_file_and_content(path::String, pattern:: String, replacement::String, :boolean) :: String
  defp rename_file_and_content(path, _, _, false) do
    path
  end

end