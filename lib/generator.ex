defmodule Generator do
  @moduledoc """
  Documentation for AppGenrator.
  """
  require Logger
  alias Generator.Utils
  
  @pattern_nom_appli "nomAppli"
  @nom_appli "gildha"
  @tmp_folder System.get_env("TMP_DIR") # |> String.trim()

  @doc """
  
  """
  def run do
    Logger.info "Application Starting"
    Logger.info "Cloning Template"


    # We get the url of the template from the system ENV
    template_url = String.trim(System.get_env("TEMPLATE_URL"))
    Logger.info "Template URL:#{template_url}"
    
    # Root path of the template
    template_path = Path.join @tmp_folder, "template"
    Logger.info "Template location:#{template_path}"

    # Create the temporary folder where the template will be clone if it doesn't exist
    unless File.exists? @tmp_folder do File.mkdir @tmp_folder end

    with {:ok, repo} <- Git.clone([template_url, template_path]), # Clone the repository
         {:ok} <- Utils.clean_git_files(repo.path), # Deleting the git files
         {:ok} <- search_and_replace(repo.path), # Rename folder and replace in all files the pattern with the given name
         {:ok, application_path} <- package(repo.path) do # Package the new application
      Logger.info "The application has been generated in the directory #{application_path}"  
      Logger.info "Cleaning..."
      File.rm_rf! template_path
    else
      {:error, reason} -> 
        Logger.error "Error:#{reason.message}"
        Logger.info "Cleaning..."
        File.rm_rf! @tmp_folder
    end 

  end

  @spec search_and_replace(:String) :: {:ok, String} | {:error, String}
  def search_and_replace(template_path) do     
      File.ls!(template_path)
      |> Enum.map(&Path.join(template_path, &1))
      |> Enum.map(&Utils.change_application_name(&1, @pattern_nom_appli, @nom_appli))
      |> Enum.filter(&File.dir? &1)
      |> Enum.map(&search_and_replace &1)
      {:ok}
  end

  def package(template_path) do
    out_folder = Path.join @tmp_folder, "out"
    File.mkdir! out_folder
    File.mkdir! Path.join(out_folder, @nom_appli)
    File.ls!(template_path)
      # TODO init git
      |> Enum.map(&copy_files(Path.join(template_path, &1), out_folder, &1, String.contains?(&1, "site")))
    {:ok, out_folder}
  end
  
  @doc """
  Copy all the files in the directory and its subdirectory
  If the condition is false it will copy inside the new application folder
  """
  @spec copy_files(:String, :String, :String, :boolean) :: binary
  def copy_files(file_path, destination, file_name, false) do
    File.cp_r! file_path, Path.join(destination, @nom_appli) 
      |> Path.join(file_name)  
  end

  @doc """
  Copy all the files in the directory and its subdirectory
  If the condition is true it will copy directly in the out folder
  """
  @spec copy_files(:String, :String, :String, :boolean) :: binary
  def copy_files(file_path, destination, file_name, true) do
    File.cp_r! file_path, Path.join(destination, file_name)
  end

end
